=======================
Party Contract Scenario
=======================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> import datetime
    >>> today = datetime.date.today()


Install party_contract::

    >>> config = activate_modules('party_contract')


Create contract::

	>>> Party = Model.get('party.party')
	>>> party = Party()
	>>> party.name = 'Party 1'
	>>> party.save()
	>>> Contract = Model.get('party.contract')
	>>> contract = Contract()
	>>> contract.party = party
	>>> contract.start_date = today
	>>> contract.save()


Create another contract with the same dates and party::

	>>> contract2 = Contract()
	>>> contract2.party = party
	>>> contract2.start_date = today
	>>> contract2.end_date = datetime.datetime.now() + datetime.timedelta(days=1)
    >>> contract2.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: The Party Contract "Party 1" overlaps with existing contract "Party 1". - 
    >>> contract2.start_date = datetime.datetime.now() + datetime.timedelta(days=2)
    >>> contract2.end_date = datetime.datetime.now() + datetime.timedelta(days=3)
    >>> contract2.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: The Party Contract "Party 1" overlaps with existing contract "Party 1". - 
    >>> contract.end_date = datetime.datetime.now() + datetime.timedelta(days=1)
    >>> contract.save()
    >>> contract2.save()


Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template1 = ProductTemplate()
    >>> template1.name = 'product1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.save()
    >>> product1, = template1.products
    >>> template2 = ProductTemplate()
    >>> template2.name = 'product2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.save()
    >>> product2, = template2.products


Check contract line product or template nulable but not both::

    >>> line = contract.lines.new()
    >>> line.start_date = today
    >>> contract.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "Product" in "Party Contract Line". - 

    >>> line.template = template1
    >>> contract.save()
    >>> line = contract.lines[0]
    >>> line.template = None
    >>> line.product = product2
    >>> contract.save()


Check contract line incoherent product and template::

    >>> line = contract.lines[0]
    >>> line.template = template1
    >>> contract.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Product" in "Party Contract Line" is not valid according to its domain. - 


Check contract line unique stardate and product or template::

    >>> contract = Contract(contract.id)
    >>> line = contract.lines.new()
    >>> line.start_date = today
    >>> line.product = product2
    >>> contract.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: A Product can only have one line by date on a Contract. - 

    >>> line.template = template2
    >>> contract.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: A Product can only have one line by date on a Contract. - 

    >>> line.template = template1
    >>> line.product = None
    >>> contract.save()
    >>> line = contract.lines.new()
    >>> line.template = template1
    >>> contract.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: A Product can only have one line by date on a Contract. - 

    >>> line.template = template2
    >>> contract.save()

Create contract with other_party::

    >>> other_party = Party()
    >>> other_party.name = 'Other Party 1'
    >>> other_party.save()
    >>> Contract = Model.get('party.contract')
    >>> contract = Contract()
    >>> contract.party = party
    >>> contract.other_party = other_party
    >>> contract.start_date = today
    >>> contract.save()

Create another contract with the same dates and party::

    >>> Contract = Model.get('party.contract')
    >>> contract = Contract()
    >>> contract.party = party
    >>> contract.other_party = other_party
    >>> contract.start_date = today
    >>> contract.save()
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: The Party Contract "Party 1" overlaps with existing contract "Party 1". - 
